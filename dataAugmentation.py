import sys
import os
import math
import numpy as np
from random import randint
from random import uniform
from scipy import ndimage

class DataAugmentor():
    def __init__(self, rotate, resize, max_num_inages=10000):
        self.rotate = rotate
        self.resize = resize
        self.max_num_inages = max_num_inages

    def start(self, foregroundImageFolder, backgroundImageFolder, targetDir):
        size = 0
        for foregroundFilename in os.listdir(foregroundImageFolder):
            path = os.path.join(foregroundImageFolder, foregroundFilename)
            rawForeground = cv2.imread(os.path.join(foregroundImageFolder, foregroundFilename), cv2.IMREAD_UNCHANGED)
            rawForeground = self.createAlphaChannel(rawForeground)
            for backgroundFilename in os.listdir(backgroundImageFolder):
                if(size >= self.max_num_inages):
                    return
                foreground = rawForeground
                if(self.resize == True):
                    foreground = self.resizeForeground(foreground)
                if(self.rotate == True):
                    foreground = self.rotateForeground(foreground)
                background = cv2.imread(os.path.join(backgroundImageFolder, backgroundFilename), cv2.IMREAD_UNCHANGED)
                # x_offset = randint(0, background.shape[1] - foreground.shape[1])
                # y_offset = randint(0, background.shape[0] - foreground.shape[0])
                # background[y_offset:y_offset + foreground.shape[0], x_offset:x_offset + foreground.shape[1]] = foreground
                
                
                # This removes tha black baground but reduces image opacity...
                background = self.augmentBackground(foreground, background)
                self.saveImage(background, os.path.join(targetDir, self.generateFilename(foregroundFilename, backgroundFilename)))
                size += 1
                #cv2.namedWindow('Display image')          ## create window for display
                #cv2.imshow('Display image', background)
                #cv2.waitKey(10000)

    def rotateForeground(self, image):
        return ndimage.rotate(image, randint(1, 359))

    def resizeForeground(self, image):
        factor = uniform(0.3, 1)
        return cv2.resize(image, (0,0), fx=factor, fy=factor) 

    def augmentBackground(self, foreground, background):
        x_offset = randint(0, background.shape[1] - foreground.shape[1])
        y_offset = randint(0, background.shape[0] - foreground.shape[0])
        
        for c in range(0,3):
            background[y_offset:y_offset + foreground.shape[0], x_offset:x_offset + foreground.shape[1], c] = \
                foreground[:, :, c] * (foreground[:, :, 3] / 255.0) +  background[y_offset:y_offset + foreground.shape[0], x_offset:x_offset + foreground.shape[1], c] * (1.0 - foreground[:,:,3] / 255.0)
        return background
            
    def saveImage(self, image, filepath):
        cv2.imwrite(filepath, image )

    def createAlphaChannel(self, image):
        if(image.shape[2] < 4):
            imageWithAlpha = np.zeros((image.shape[0], image.shape[1], image.shape[2] + 1))
            for i0 in range(0, image.shape[0]):
                for i1 in range(0, image.shape[1]):
                    for i2 in range(0, image.shape[2] + 1):
                        if (i2 == 3):
                            imageWithAlpha[i0, i1, i2] = 255
                        else:
                            imageWithAlpha[i0, i1, i2] = image[i0, i1 ,i2]
            return imageWithAlpha
        return image
            
    def generateFilename(self, foregroundFilename, backgroundFilename):
        split = foregroundFilename.split(".")
        return split[0] + "_" + backgroundFilename

if __name__ == "__main__":
    startProgramm = True
    if len(sys.argv) < 3:
        print("Usage : python dataAugmentation.py <foreground_image_dir> <background_image_dir> <target_dir>")
        startProgramm = False
    try:
        import cv2
    except:
        startProgramm = False
        print("OpenCV ist not installed on your machine! Please install it before running this programm.")
    
    if(startProgramm):
        augmentor = DataAugmentor(True, True)
        augmentor.start(sys.argv[1], sys.argv[2], sys.argv[3])
