from __future__ import print_function

import json
import cv2
import os, glob
import numpy as np
import numpy.random as rand
from utils import *
import pickle

import sys
import os
import time

import theano
import theano.tensor as T

import lasagne
from lasagne.layers import set_all_param_values



# x,y,z,k = load_dataset()
# print(x)
# print(y)
# print(z)
# print(k)


###########

def build_cnn(input_var=None, nfilters = 32):
    # As a third model, we'll create a CNN of two convolution + pooling stages
    # and a fully-connected hidden layer in front of the output layer.

    # Input layer, as usual:
    network = lasagne.layers.InputLayer(shape=(None, 3, 224, 224),
                                        input_var=input_var)
    # This time we do not apply input dropout, as it tends to work less well
    # for convolutional layers.

    # Convolutional layer with 32 kernels of size 5x5. Strided and padded
    # convolutions are supported as well; see the docstring.
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=nfilters, filter_size=(5, 5),
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())
    # Expert note: Lasagne provides alternative convolutional layers that
    # override Theano's choice of which implementation to use; for details
    # please see http://lasagne.readthedocs.org/en/latest/user/tutorial.html.

    # Max-pooling layer of factor 2 in both dimensions:
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))

    # Another convolution with 32 5x5 kernels, and another 2x2 pooling:
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=nfilters, filter_size=(5, 5),
            nonlinearity=lasagne.nonlinearities.rectify)
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))

    # A fully-connected layer of 256 units with 50% dropout on its inputs:
    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=256,
            nonlinearity=lasagne.nonlinearities.rectify)

    # And, finally, the 10-unit output layer with 50% dropout on its inputs:
    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=1,
            nonlinearity=lasagne.nonlinearities.softmax)

    return network


# ############################# Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.
# Notice that this function returns only mini-batches of size `batchsize`.
# If the size of the data is not a multiple of `batchsize`, it will not
# return the last (remaining) mini-batch.

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


# ############################## Main program ################################
# Everything else will be handled in our main program now. We could pull out
# more functions to better separate the code, but it wouldn't make it any
# easier to read.

def number_batches(data, batch_size):
    n_samples = data.shape[0]
    n_batches = n_samples // batch_size
    if ((n_samples % batch_size) <> 0) :
        n_batches = n_batches + 1
    return n_batches

def save_parameters(params):
    output = open('NN_small_params.pkl', 'wb')
    pickle.dump(params, output)
    output.close()
    
class FishNoFishClasifier():
    def __init__(self):
        self.output_layer = self.init()

    def fowardPass(self, X):
        return lasagne.layers.get_output(self.output_layer, X, deterministic=True).eval()

    def init(self):
        output_layer = build_cnn()
        model = pickle.load(open('NN_small_params.pkl', 'rb'))

        set_all_param_values(output_layer, model)
        
        return output_layer


def main(ntrain = 50000, nvalid = 10000, ntest = 10000, algorithm_type = 1,
         batch_size_train = 500, batch_size_valid = 500, batch_size_test = 500,
         num_epochs=500, LR = 0.1,
         nfilters = 32, time_limit = 10000):
   
    # Prepare Theano variables for inputs and targets
    input_var = T.tensor4('inputs')
    target_var = T.fmatrix('targets')

    # Load the dataset
    print("Loading data...")
    output_layer = build_cnn(input_var)

    img_in , img_lab , img_name, img_test = load_dataset()
    #names = load_names()
   
    img_in = img_in.astype(np.float32)
    img_in = np.divide(img_in, img_in.max())
    all_features = []
    all_labels = img_lab
    all_names = img_name

    img_lab = (img_lab != 4)*1 # *1 to get 0,1 as labels instead of True and False...Yes it's stupid...
    #img_lab = np.zeros((img_lab_1.shape[0], 1))
    #img_lab[:, 0] = img_lab_1
    #img_lab[:, 1] = (img_lab[:, 0] != 1)*1
    
    for train_idxs, val_idxs in get_cv_splits(img_name, nsplits=2):

        #print("train indx : ",np.shape(train_idxs))
        #print("validation indx : ",np.shape(val_idxs))
        np.random.shuffle(train_idxs)
        np.random.shuffle(val_idxs)
        train_img_in = img_in [train_idxs]
        train_img_lab = img_lab [train_idxs]
        val_img_in = img_in [val_idxs]
        val_img_lab = img_lab [val_idxs]
        print("train shape : ",train_img_lab.shape)
        print("validation shape : ",val_img_lab.shape)
        #idx = int(0.1*np.shape(val_img_in)[0])
        #val_img_in = val_img_in[:idx,...]
        #val_img_lab = val_img_lab[:idx,...]
        # Create a loss expression for training, i.e., a scalar objective we want
        # to minimize (for our multi-class problem, it is the cross-entropy loss):
        prediction = lasagne.layers.get_output(output_layer)
        loss = lasagne.objectives.binary_crossentropy(prediction, target_var)
        #loss = lasagne.objectives.aggregate(lasagne.objectives.squared_error(prediction, target_var), mode='mean')
        loss = loss.mean()
        # We could add some weight decay as well here, see lasagne.regularization.

        # Create update expressions for training, i.e., how to modify the
        # parameters at each training step. 
        NN_params = lasagne.layers.get_all_params(output_layer, trainable=True)
        updates = lasagne.updates.adam(loss, NN_params, learning_rate=LR)

        # Create a loss expression for validation/testing. The crucial difference
        # here is that we do a deterministic forward pass through the output_layer,
        # disabling dropout layers.
        test_prediction = lasagne.layers.get_output(output_layer, deterministic=True)
        test_loss = lasagne.objectives.binary_crossentropy(test_prediction,
                                                                target_var)
        #test_loss = lasagne.objectives.aggregate(lasagne.objectives.squared_error(test_prediction, target_var), mode='mean')        
        test_loss = test_loss.mean()
        
        # As a bonus, also create an expression for the classification accuracy:
        # test_acc = T.mean(T.eq(T.argmax(T.round(test_prediction.T), axis=1), target_var),
        #                  dtype=theano.config.floatX)

        # Compile a function performing a training step on a mini-batch (by giving
        # the updates dictionary) and returning the corresponding training loss:
        train_fn = theano.function([input_var, target_var], loss , updates=updates)
        params_fn = theano.function([], outputs=NN_params)
        # Compile a second function computing the validation loss and accuracy:
        val_fn = theano.function([input_var, target_var], [test_loss ,  test_prediction])

        # Finally, launch the training loop.
        nparameters = lasagne.layers.count_params(output_layer, trainable=True)
        print("Number of parameters in model: {}".format(nparameters))
        print("Starting training...")

        best_val_acc = 0
        img_in = train_img_in
        img_lab = train_img_lab
        
        
        # We iterate over epochs:
        for epoch in range(num_epochs):
            # In each epoch, we do a full pass over the training data:
            train_err = 0
            train_accuracy = 0
            train_batches = 0
            batch_size = 40
            n_batches = number_batches(img_in,batch_size)
            start_time_epoch = time.time()
            for b in range(n_batches):
                #print(b)
                batch_begin = b*batch_size
                if ((batch_begin+batch_size)>img_in.shape[0]):
                    batch_end = img_in.shape[0]
                else :
                    batch_end = batch_begin+batch_size
                img_in_batch = img_in[batch_begin:batch_end]
                img_lab_batch = img_lab[batch_begin:batch_end]
            #print(np.shape(img_lab_batch.reshape((img_lab_batch.shape[0], 1)).astype(np.uint8)))
                err = train_fn(img_in_batch, img_lab_batch.reshape((img_lab_batch.shape[0], 1)).astype(np.uint8))
                train_err += err
                #train_accuracy += compute_accuracy(train_pred, img_lab_batch , threshold=0.5, num_output=2)
                train_batches += 1        


            val_err = 0
            val_accuracy = 0
            batch_size = 20
            val_batches = 0
            n_batches = number_batches(val_img_in,batch_size)
            for b in range(n_batches):
                batch_begin = b*batch_size
                if ((batch_begin+batch_size)>val_img_in.shape[0]):
                    batch_end = val_img_in.shape[0]
                else :
                    batch_end = batch_begin+batch_size
                val_img_in_batch = val_img_in[batch_begin:batch_end]
                val_img_lab_batch = val_img_lab[batch_begin:batch_end]
                #print(val_img_in_batch.shape , val_img_lab_batch.shape)
            #print(np.shape(val_img_lab_batch.reshape((val_img_lab_batch.shape[0], 1)).astype(np.uint8)))
                err , predict = val_fn(val_img_in_batch, val_img_lab_batch.reshape((val_img_lab_batch.shape[0], 1)).astype(np.uint8))
                val_err += err
            #print(predict)
            #print(val_img_lab_batch)
                #val_accuracy += acc
            val_accuracy += compute_accuracy(predict, val_img_lab_batch.reshape((val_img_lab_batch.shape[0], 1)) , threshold=0.5, num_output=1)
            val_batches += 1

            # Then we print the results for this epoch:
            print("Epoch {} of {} took {:.3f}s".format(
                epoch + 1, num_epochs, time.time() - start_time_epoch))
            print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
            #print("  training accuracy:\t\t{:.2f} %".format(float(train_accuracy / train_batches * 100)))
            print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
            print("  validation accuracy:\t\t{:.2f} %".format(float(val_accuracy / val_batches * 100)))

            #if (val_acc / val_batches * 100 > best_val_acc):
            #    best_val_acc = val_acc / val_batches * 100

        #return [best_val_acc, nparameters]
            NN_parameters = params_fn()
            save_parameters(NN_parameters)
            
    

def compute_error(Y_pred, Y , num_output, threshold=0.5 ):
    Y_pred_a = np.asarray(Y_pred)
    Y_a = np.asarray(Y)

    if (num_output == 1):
        shape2_Y_pred_a = 1
        shape2_Y_a = 1
    else :
        shape2_Y_pred_a = np.shape(Y_pred_a)[1]
        shape2_Y_a = np.shape(Y_a)[1]

    Y_pred_a = np.reshape(Y_pred_a , (np.shape(Y_pred_a)[0] ,shape2_Y_pred_a))
    indx = Y_pred_a <= threshold
    Y_pred_a [indx] = 0
    Y_pred_a [~indx] = 1
    Y_a = np.reshape(Y_a , (np.shape(Y_a)[0] ,shape2_Y_a))
    compare_result = abs(Y_pred_a - Y_a)
    return compare_result.mean(axis=0)


def compute_accuracy(Y_pred, Y , threshold=0.5 , num_output=1):
    err = compute_error(Y_pred, Y , num_output, threshold )
    return 1-err

#
if __name__ == '__main__':

    ntrain = 100              # number of training examples
    nvalid = 100              # number of validation examples
    ntest = 100               # number of test examples
    batch_size_train = 50      # batch size used for training
    batch_size_valid = 50      # batch size used for validation
    batch_size_test = 50       # batch size used for testing
    algorithm_type = 1          # 1 - SGD, 2 - SGD with momentum

    nfilters = 32               # number of convolutional filters in each layer
    time_limit = 10000          # .. seconds, huge value to disable this option

    ntrain = 5000           # reduce the size of the training set by a factor of 10 to save 10x time
    nvalid = 1000           # same here
    ntest = 1000            # same here
    num_epochs = 5

    irun = 1              # independent runs
    LR = 0.001            # different settings for learning rate LR of SGD
    
    main(ntrain, nvalid, ntest, algorithm_type, batch_size_train,
            batch_size_valid, batch_size_test, num_epochs, LR, nfilters, time_limit)

