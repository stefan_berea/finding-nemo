import time

import numpy as np
import theano
import theano.tensor as T
import lasagne
import pickle
from utils import *

from lasagne.layers import DenseLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import InputLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.layers import NonlinearityLayer
# from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.layers import Conv2DLayer as ConvLayer
from lasagne.layers import MaxPool2DLayer as PoolLayer
from lasagne.nonlinearities import sigmoid
from lasagne.layers import set_all_param_values

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]

#def load_dataset():
#    features = np.load('features.out.npy')
#    #features = np.reshape(features,(np.shape(features)[0],np.shape(features)[1]*np.shape(features)[2]*np.shape(features)[3]))
#    labels = np.load('labels.out.npy')
#    return features, labels

class Network:
    def build_cnn(self, input_var=None):
        net = {}

        net = InputLayer((None, 3, 224, 224), input_var=input_var)
        net = ConvLayer(net, num_filters=96,
                        filter_size=7,
                         stride=2)
        # caffe has alpha = alpha * pool_size
        net = NormLayer(net, alpha=0.0001)
        net = PoolLayer(net, pool_size=3,
                                 stride=3,
                                 ignore_border=False)
        net = ConvLayer(net, num_filters=256,
                                 filter_size=5)
        net = PoolLayer(net, pool_size=2,
                                 stride=2,
                                 ignore_border=False)
        net = ConvLayer(net, num_filters=512,
                                 filter_size=3,
                                 pad=1)
        net = ConvLayer(net, num_filters=512,
                                 filter_size=3,
                                 pad=1)
        net = ConvLayer(net, num_filters=512,
                                 filter_size=3,
                                 pad=1)
        net = PoolLayer(net, pool_size=3,
                                 stride=3,
                                 ignore_border=False)
        net = DenseLayer(net, num_units=4096)
        net = DropoutLayer(net, p=0.5)
        net = DenseLayer(net, num_units=4096)
        net = DropoutLayer(net, p=0.5)
        net = DenseLayer(net, num_units=1000, nonlinearity=None)
        net = DenseLayer(net, num_units=2, nonlinearity=sigmoid)

        return net
            
    def number_batches(self, data, batch_size):
        n_samples = data.shape[0]
        n_batches = n_samples // batch_size
        if ((n_samples % batch_size) <> 0) :
            n_batches = n_batches + 1
        return n_batches

    def main(self, num_epochs=20, LR = 0.001, time_limit = 10000):
        # Prepare Theano variables for inputs and targets
        input_var = T.tensor4('inputs')
        target_var = T.matrix('targets')

        # Load the dataset
        print("Loading data...")
        network = self.build_cnn(input_var)
        model = pickle.load(open('vgg_cnn_s.pkl'))
        CLASSES = model['synset words']
        MEAN_IMAGE = model['mean image']


        img_in , img_lab_1 , img_name , img_test = load_dataset()
        batch_size = 40
        n_batches = self.number_batches(img_in,batch_size)
        img_in = img_in.astype(np.float32)
        img_in = np.divide(img_in, img_in.max())
        all_features = []
        all_labels = img_lab_1
        all_names = img_name

        img_lab_1 = (img_lab_1 != 1)*1 # *1 to get 0,1 as labels instead of True and False...Yes it's stupid...
        img_lab = np.zeros((img_lab_1.shape[0], 2))
        img_lab[:, 0] = img_lab_1
        img_lab[:, 1] = (img_lab[:, 0] != 1)*1
        print(img_lab)
        
        # Create a loss expression for training, i.e., a scalar objective we want
        # to minimize (for our multi-class problem, it is the cross-entropy loss):
        prediction = lasagne.layers.get_output(network)
        loss = lasagne.objectives.categorical_crossentropy(prediction, target_var, )
        loss = loss.mean()
        # We could add some weight decay as well here, see lasagne.regularization.

        # Create update expressions for training, i.e., how to modify the
        # parameters at each training step. 
        params = lasagne.layers.get_all_params(network, trainable=True)
        updates = lasagne.updates.adam(loss, params, learning_rate=LR)

        # Create a loss expression for validation/testing. The crucial difference
        # here is that we do a deterministic forward pass through the network,
        # disabling dropout layers.
        #test_prediction = lasagne.layers.get_output(network, deterministic=True)
        #test_loss = lasagne.objectives.binary_crossentropy(test_prediction,
        #                                                        target_var)
        #test_loss = test_loss.mean()
        # As a bonus, also create an expression for the classification accuracy:
        #test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
         #                 dtype=theano.config.floatX)

        # Compile a function performing a training step on a mini-batch (by giving
        # the updates dictionary) and returning the corresponding training loss:
        train_fn = theano.function([input_var, target_var], loss, updates=updates)

        # Compile a second function computing the validation loss and accuracy:
        #val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

        # Finally, launch the training loop.
        nparameters = lasagne.layers.count_params(network, trainable=True)
        print("Number of parameters in model: {}".format(nparameters))
        print("Starting training...")

        #best_val_acc = 0

        # We iterate over epochs:
        for epoch in range(num_epochs):
            # In each epoch, we do a full pass over the training data:
            train_err = 0
            train_batches = 0
            start_time_epoch = time.time()
            for b in range(n_batches):
                print(b)
                batch_begin = b*batch_size
                if ((batch_begin+batch_size)>img_in.shape[0]):
                    batch_end = img_in.shape[0]
                else :
                    batch_end = batch_begin+batch_size
                img_in_batch = img_in[batch_begin:batch_end]
                img_lab_batch = img_lab[batch_begin:batch_end]
                train_err += train_fn(img_in_batch, img_lab_batch.reshape((img_lab_batch.shape[0], 2)).astype(np.float32))
                train_batches += 1        

            # And a full pass over the validation data:
            #val_err = 0
            #val_acc = 0
            #val_batches = 0
            #for batch in iterate_minibatches(X_val, y_val, batch_size_valid, shuffle=False):
            #    inputs, targets = batch
            #    err, acc = val_fn(inputs, targets)
            #    val_err += err
            #    val_acc += acc
            #    val_batches += 1

            # Then we print the results for this epoch:
            print("Epoch {} of {} took {:.3f}s".format(
                epoch + 1, num_epochs, time.time() - start_time_epoch))
            print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
            #print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
            #print("  validation accuracy:\t\t{:.2f} %".format(
            #    val_acc / val_batches * 100))

            #if (val_acc / val_batches * 100 > best_val_acc):
            #    best_val_acc = val_acc / val_batches * 100

        #return [best_val_acc, nparameters]

if __name__ == '__main__':
    net = Network()
    net.main()

