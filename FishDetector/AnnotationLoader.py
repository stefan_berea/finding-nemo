import json
import cv2
import os, glob
import re
import numpy as np


class AnnotationCoordinates():
    def __init__(self, dictCoordinates):
        self.label = str(dictCoordinates['class'])
        self.height = int(dictCoordinates['height'])
        self.width = int(dictCoordinates['width'])
        self.x = int(dictCoordinates['x'])
        self.y = int(dictCoordinates['y'])
        self.boundingBox = np.array([[self.x, self.y],[self.y + self.height, self.x + self.width]])

class Annotation():
    def __init__(self, dictAnnotation):
        self.label = str(dictAnnotation['class'])
        self.filename = str(dictAnnotation['filename'])
        self.annotations = [AnnotationCoordinates(coordinate) for coordinate in dictAnnotation['annotations']]
        
    def getBBoxes(self):
        bboxes = np.zeros((len(self.annotations), 2, 2))
        for i in range(len(self.annotations)):
            bboxes[i] = self.annotations[i].boundingBox
        return bboxes
        

class AnnotationLoader():
    
    def __init__(self):
        self.load()
    
    ###
    def getFilesInFolder(self, folderName):
        return sorted(glob.glob(folderName + '/*'))

    ###
    def readJSONFile(self, filePath):
        with open(filePath) as json_data:
            jsonData = json.load(json_data)
            return jsonData

    ###
    def load(self):
        files = self.getFilesInFolder("boundingBoxes")
        self.annotations = {}
        for i in range(len(files)):
            label = re.search('boundingBoxes/(.+?).json', files[i])
            if(label):
                self.annotations[label.group(1)] = self.readJSONFile(files[i])

    
    '''
        Reqires input like: 'ALB/img_00003.jpg'
        Returns annotations for this image or None if no annotation exists
    '''
    def getAnnotationByFilename(self, filename):
        parts = filename.split('/')
        for i in range(len(self.annotations[parts[0]])):
            if (self.annotations[parts[0]][i]['filename'] == filename):
                return Annotation(self.annotations[parts[0]][i])
        return None
