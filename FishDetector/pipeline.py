from utils import *
from crop import *
import cv2
from Metrics import *

img_in, names = load_images(["ALB", "BET", "DOL", "LAG", "NoF", "OTHER", "SHARK", "YFT"])

print('start training SVM')
svm = SVM().train()
print('finished training SVM')

cropper = ImageCrop(svm)
predictions = []

num_samples = 10

#cropper.getBoundingBox(cv2.imread('data/ALB/img_00003.jpg')) #''data/NoF/img_00008.jpgimg_00491'data/BET/img_00531.jpg'
#'data/ALB/img_00634.jpg''data/ALB/img_00638.jpg'
for i in range(num_samples): #img_in.shape[0]):
    print('Crop image', names[i])
    img_coordinates = np.array([[0,0], [img_in[i].shape[0], img_in[i].shape[1]]])
    #outputs, coordinates = cropper.getBoundingBoxes(img_in[i], img_coordinates)
    coordinates = RandomBoxes().getPredictions(img_in[i])
    predictions.append(Prediction(names[i], np.array(coordinates)))
    #for j in range(len(outputs)):
        #cv2.imwrite("./thumbnails/thumbnail" + str(i) + "_" + str(j) + ".png", outputs[i])
    print('Finished crop image')
    
print('Cropping performance:', ObjectDetectorPrecisison().calculate(predictions) / num_samples)



#cropper.getBoundingBoxes(cv2.imread('data/YFT/img_02958.jpg'))
#cropper.getBoundingBoxes(cv2.imread('data/YFT/img_02962.jpg'))
#cropper.getBoundingBoxes(cv2.imread('data/YFT/img_02966.jpg'))

# metric - done
# croppingt - done
# no fish probabilities - open
# baseline - done
# s2 classifier - in progress
# rcnn - unkown (in progress?!?)
