import time

import numpy as np
import theano
import theano.tensor as T
import lasagne
import pickle
from utils import *

from lasagne.layers import DenseLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import InputLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.layers import NonlinearityLayer
# from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.layers import Conv2DLayer as ConvLayer
from lasagne.layers import MaxPool2DLayer as PoolLayer
from lasagne.nonlinearities import sigmoid
from lasagne.layers import set_all_param_values

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]

#def load_dataset():
#    features = np.load('features.out.npy')
#    #features = np.reshape(features,(np.shape(features)[0],np.shape(features)[1]*np.shape(features)[2]*np.shape(features)[3]))
#    labels = np.load('labels.out.npy')
#    return features, labels

class Network:
    def build_cnn(self, input_var=None):
        net = {}

        net['input'] = InputLayer((None, 3, 224, 224), input_var=input_var)
        net['conv1'] = ConvLayer(net['input'],
                                num_filters=96,
                                filter_size=7,
                                stride=2)
        # caffe has alpha = alpha * pool_size
        net['norm1'] = NormLayer(net['conv1'], alpha=0.0001)
        net['pool1'] = PoolLayer(net['norm1'],
                                pool_size=3,
                                stride=3,
                                ignore_border=False)
        net['conv2'] = ConvLayer(net['pool1'],
                                num_filters=256,
                                filter_size=5)
        net['pool2'] = PoolLayer(net['conv2'],
                                pool_size=2,
                                stride=2,
                                ignore_border=False)
        net['conv3'] = ConvLayer(net['pool2'],
                                num_filters=512,
                                filter_size=3,
                                pad=1)
        net['conv4'] = ConvLayer(net['conv3'],
                                num_filters=512,
                                filter_size=3,
                                pad=1)
        net['conv5'] = ConvLayer(net['conv4'],
                                num_filters=512,
                                filter_size=3,
                                pad=1)
        net['pool5'] = PoolLayer(net['conv5'],
                                pool_size=3,
                                stride=3,
                                ignore_border=False)
        net['fc6'] = DenseLayer(net['pool5'], num_units=4096)
        net['drop6'] = DropoutLayer(net['fc6'], p=0.5)
        net['fc7'] = DenseLayer(net['drop6'], num_units=4096)
        net['drop7'] = DropoutLayer(net['fc7'], p=0.5)
        net['fc8'] = DenseLayer(net['drop7'], num_units=1000, nonlinearity=None)
        net['prob'] = NonlinearityLayer(net['fc8'], sigmoid)

        return net
            
    def number_batches(self, data, batch_size):
        n_samples = data.shape[0]
        n_batches = n_samples // batch_size
        if ((n_samples % batch_size) <> 0) :
            n_batches = n_batches + 1
        return n_batches
    
    def save_parameters(self, params):
        output = open('NN_params.pkl', 'wb')
        pickle.dump(params, output)
        output.close()
        
        
    def main(self, num_epochs=5, LR = 0.01, time_limit = 10000):
        # Prepare Theano variables for inputs and targets
        input_var = T.tensor4('inputs')
        target_var = T.matrix('targets')

        # Load the dataset
        print("Loading data...")
        network = self.build_cnn(input_var)
        
        model = pickle.load(open('vgg_cnn_s.pkl'))
        CLASSES = model['synset words']
        MEAN_IMAGE = model['mean image']
        layers = lasagne.layers.get_all_layers(network['prob'])
        
        set_all_param_values(layers, model['values'])
        output_layer = DenseLayer(network['fc8'], num_units=2, nonlinearity=sigmoid)

        img_in , img_lab_1 , img_name , img_test = load_dataset()
        
        img_in = img_in.astype(np.float32)
        img_in = np.divide(img_in, img_in.max())
        all_features = []
        all_labels = img_lab_1
        all_names = img_name

        img_lab_1 = (img_lab_1 != 4)*1 # *1 to get 0,1 as labels instead of True and False...Yes it's stupid...
        img_lab = np.zeros((img_lab_1.shape[0], 2))
        img_lab[:, 0] = img_lab_1
        img_lab[:, 1] = (img_lab[:, 0] != 1)*1
        print(img_lab)
        
        for train_idxs, val_idxs in get_cv_splits(img_name, nsplits=2):

            np.random.shuffle(train_idxs)
            np.random.shuffle(val_idxs)
            train_img_in = img_in [train_idxs]
            train_img_lab = img_lab [train_idxs]
            val_img_in = img_in [val_idxs]
            val_img_lab = img_lab [val_idxs]
            print("train shape : ",train_img_lab.shape)
            print("validation shape : ",val_img_lab.shape)
            
            # Create a loss expression for training, i.e., a scalar objective we want
            # to minimize (for our multi-class problem, it is the cross-entropy loss):
            prediction = lasagne.layers.get_output(output_layer)
            #loss = lasagne.objectives.categorical_crossentropy(prediction, target_var, )
            loss = lasagne.objectives.aggregate(lasagne.objectives.squared_error(prediction, target_var), mode='mean')
            #loss = loss.mean()
            # We could add some weight decay as well here, see lasagne.regularization.

            # Create update expressions for training, i.e., how to modify the
            # parameters at each training step. 
            NN_params = lasagne.layers.get_all_params(output_layer, trainable=True)
            updates = lasagne.updates.adam(loss, NN_params, learning_rate=LR)

            # Create a loss expression for validation/testing. The crucial difference
            # here is that we do a deterministic forward pass through the output_layer,
            # disabling dropout layers.
            test_prediction = lasagne.layers.get_output(output_layer, deterministic=True)
            test_loss = lasagne.objectives.binary_crossentropy(test_prediction,
                                                                    target_var)
            #test_loss = lasagne.objectives.aggregate(lasagne.objectives.squared_error(test_prediction, target_var), mode='mean')        
            #test_loss = test_loss.mean()
            # As a bonus, also create an expression for the classification accuracy:
            #test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
            #                  dtype=theano.config.floatX)

            # Compile a function performing a training step on a mini-batch (by giving
            # the updates dictionary) and returning the corresponding training loss:
            train_fn = theano.function([input_var, target_var], loss, updates=updates)
            params_fn = theano.function([], outputs=NN_params)
            # Compile a second function computing the validation loss and accuracy:
            val_fn = theano.function([input_var, target_var], test_loss)

            # Finally, launch the training loop.
            nparameters = lasagne.layers.count_params(output_layer, trainable=True)
            print("Number of parameters in model: {}".format(nparameters))
            print("Starting training...")

            best_val_acc = 0
            img_in = train_img_in
            img_lab = train_img_lab
            
            
            # We iterate over epochs:
            for epoch in range(num_epochs):
                # In each epoch, we do a full pass over the training data:
                train_err = 0
                train_batches = 0
                batch_size = 40
                n_batches = self.number_batches(img_in,batch_size)
                start_time_epoch = time.time()
                for b in range(n_batches):
                    print(b)
                    batch_begin = b*batch_size
                    if ((batch_begin+batch_size)>img_in.shape[0]):
                        batch_end = img_in.shape[0]
                    else :
                        batch_end = batch_begin+batch_size
                    img_in_batch = img_in[batch_begin:batch_end]
                    img_lab_batch = img_lab[batch_begin:batch_end]
                    train_err += train_fn(img_in_batch, img_lab_batch.reshape((img_lab_batch.shape[0], 2)).astype(np.float32))
                    train_batches += 1        


                val_err = 0
                val_acc = 0
                batch_size = 20
                val_batches = 0
                n_batches = self.number_batches(val_img_in,batch_size)
                for b in range(n_batches):
                    print(b)
                    batch_begin = b*batch_size
                    if ((batch_begin+batch_size)>val_img_in.shape[0]):
                        batch_end = val_img_in.shape[0]
                    else :
                        batch_end = batch_begin+batch_size
                    val_img_in_batch = val_img_in[batch_begin:batch_end]
                    val_img_lab_batch = val_img_lab[batch_begin:batch_end]
                    #print(val_img_in_batch.shape , val_img_lab_batch.shape)
                    err = val_fn(val_img_in_batch, val_img_lab_batch.reshape((val_img_lab_batch.shape[0], 2)).astype(np.float32))
                    val_err += err
                    #val_acc += acc
                    val_batches += 1

                # Then we print the results for this epoch:
                print("Epoch {} of {} took {:.3f}s".format(
                    epoch + 1, num_epochs, time.time() - start_time_epoch))
                print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
                print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
                #print("  validation accuracy:\t\t{:.2f} %".format(
                #    val_acc / val_batches * 100))

                #if (val_acc / val_batches * 100 > best_val_acc):
                #    best_val_acc = val_acc / val_batches * 100

            #return [best_val_acc, nparameters]
                NN_parameters = params_fn()
                self.save_parameters(NN_parameters)
        
if __name__ == '__main__':
    net = Network()
    net.main()

