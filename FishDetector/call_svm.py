from collections import Counter
import numpy as np
#import matplotlib.pyplot as plt
import pickle
import io
import math
import random
#import skimage.transform
from sklearn import svm,ensemble
from sklearn.metrics import accuracy_score, log_loss, f1_score
from sklearn.metrics import confusion_matrix
from utils import *

features = np.load('features.out.npy')
features = np.reshape(features,(np.shape(features)[0],np.shape(features)[1]))
labels = np.load('labels.out.npy')
print(np.shape(features))

names = load_names()

# seperating train and test samples for SVM
total_samples = np.shape(features)[0]
n_train = 0.8
n_test = 0.2
n_train_samples = int(math.floor(total_samples*n_train))
av_acc = 0.0
most_common_av_acc = 0.0
av_cross_ent = 0.0
l = 0

for train_idxs, val_idxs in get_cv_splits(names, nsplits=10):

    train_x = features[train_idxs, ...]
    train_y = labels[train_idxs, ...]
    test_x = features[val_idxs, ...]
    test_y = labels[val_idxs, ...]

    train_y = (train_y != 4)*1
    test_y = (test_y != 4)*1
    #print(test_y)

    mc = Counter(train_y).most_common(1)[0][0]
    mc_count = Counter(train_y).most_common(1)[0][1]
    print mc, mc_count
    #most_common_acc = np.sum(mc == test_y)/float(len(test_y))
    print ("val size = ", len(val_idxs))
    num = int(mc_count/float(len(train_y)) * (len(test_y)))
    pred = [1-int(mc)]*num
    pred.extend([int(mc)]*(len(test_y) - num))
    pred = np.array(pred)
    
    np.random.shuffle(pred)

    lin_clf = svm.LinearSVC(class_weight={False: 7, True: 1})
    lin_clf.fit(train_x, train_y)
    p_test_y = lin_clf.predict(test_x)

    accuracy = f1_score(test_y, p_test_y)
    most_common_acc = f1_score(test_y, np.random.randint(2, size=len(test_y)))
    conf_m = confusion_matrix(test_y, p_test_y)
    print ("most_common_acc (random) = ", most_common_acc)
    print("accuracy = {:f}".format(accuracy))
    print(conf_m)
	
    #print("cross_entropy = {:f}".format(cross_ent))
    av_acc += accuracy*len(test_y)
    most_common_av_acc += most_common_acc*len(test_y)
    l += len(test_y)

print "Average:", av_acc/l
print "Most common:", most_common_av_acc/l
