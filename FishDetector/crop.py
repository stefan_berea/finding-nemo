import cv2
import numpy as np
from utils import *
from VGG_S import *
import os.path
from sklearn import svm

class SVM():
    
    def __init__(self):
        self.isTrained = os.path.exists('SVM_params.pkl')
        if(self.isTrained):
            print('SVM: load trained model')
            self.model = pickle.load(open('SVM_params.pkl'))
        else:
            self.model = svm.LinearSVC(class_weight={0: 10})
        
    def train(self):
        #labels = (labels != 4)*1
        if(not self.isTrained):
            
            features = np.load('features.out.npy')
            print(np.shape(features))
            features = np.reshape(features,(np.shape(features)[0],np.shape(features)[1]*np.shape(features)[2]*np.shape(features)[3]))
            labels = np.load('labels.out.npy')
            
            labels[labels != 4] = 1
            labels[labels == 4] = 0
            
            self.model.fit(features, labels)
            self.save_parameters()
        else:
            print('SVM: --> Skip training')
        return self
    
    def save_parameters(self):
        output = open('SVM_params.pkl', 'wb')
        pickle.dump(self.model, output)
        output.close()
        
    def predict(self, X):
        return self.model.decision_function(X)


class ImageCrop():
    def __init__(self, svm):
        self.svm = svm
        self.imageNet = ImageNet()
        self.thumbs = 0
        self.max_depth = 4
        self.depth_dependence = 1

    def getBoundingBoxes(self, img, original_coordinates, threshold=None, depth=0):
        
        
        if(depth > self.max_depth or img.shape[0] < 224 or img.shape[1] < 224):
            print('depth exceeded / img too small', depth, img.shape)
            return [], []
        try:
            dist, labels = self.getFishProbability([img])
        except:
            return [], []
        if((threshold != None and dist[0] < threshold)):
            print('Not good enough', dist[0], threshold)
            return [], []
        
        
        print(depth, 'start cropping')
        
        candidates = self.getCandidates(img, depth)
        
        images = []
        coordinates = []
        
        for i in range(candidates.shape[0]):
            cropped_img = img[candidates[i,0,0]:candidates[i,1,0], candidates[i,0,1]:candidates[i,1,1]]
            candidate_coordinates = np.array([[candidates[i,0,0], candidates[i,0,1]], [candidates[i,1,0], candidates[i,1,1]]])
            images_new, coordinates_new = self.getBoundingBoxes(cropped_img, candidate_coordinates, dist[0], depth + 1)
            
            images += images_new
            coordinates += coordinates
            
        if(images == []):
            print(depth, 'FUUUUUUCK')
            return [], [original_coordinates]
        else:
            return [], self.translateCoordinates(original_coordinates, coordinates)
        
    def translateCoordinates(self, original_coordinates, candidate_coordinates):
        for i in len(candidate_coordinates):
            candidate_coordinates[i] += original_coordinates

    def getCandidates(self, img, depth):
        rnd_number = np.random.random_sample()
        img_w_center = img.shape[1] / 2
        img_h_center = img.shape[0] / 2
        img_w_margin = int(img.shape[1] / 2) + (int(img.shape[1] / 2) * rnd_number)
        img_h_margin = int(img.shape[0] / 2) + (int(img.shape[0] / 2) * rnd_number)

        candidates = np.zeros((4,2,2))

#        candidates[0, 0] = [0,0]
#        candidates[0, 1] = [img_h_center + img_h_margin, img_w_center + img_w_margin]

 #       candidates[1, 0] = [0, img_w_center - img_w_margin]
  #      candidates[1, 1] = [img_h_center + img_h_margin, img.shape[1]]

   #     candidates[2, 0] = [img_h_center - img_h_margin,0]
    #    candidates[2, 1] = [img.shape[0], img_w_center + img_w_margin]

     #   candidates[3, 0] = [img_h_center - img_h_margin, img_w_center - img_w_margin]
      #  candidates[3, 1] = [img.shape[0], img.shape[1]]
        
        ###
        
        #candidates[0, 0] = [img.shape[0] - img_h_margin, img.shape[1] - img_w_margin]
        #candidates[0, 1] = [img.shape[0], img.shape[1]]

#        candidates[1, 0] = [img.shape[0] - img_h_margin, 0]
 #       candidates[1, 1] = [img.shape[0], img_w_margin]

  #      candidates[2, 0] = [img_h_margin, img.shape[1] - img_w_margin]
   #     candidates[2, 1] = [0, img.shape[1]]

    #    candidates[3, 0] = [0, 0]
     #   candidates[3, 1] = [img_h_margin, img_w_margin]
        
        ###
        
        candidates[0, 0] = [0, 0]
        candidates[0, 1] = [img_h_margin, img.shape[1]]

        candidates[1, 0] = [0, 0]
        candidates[1, 1] = [img.shape[0], img_w_margin]

        candidates[2, 0] = [0, img.shape[1] - img_w_margin]
        candidates[2, 1] = [img.shape[0], img.shape[1]]

        candidates[3, 0] = [img.shape[0]- img_h_margin, 0]
        candidates[3, 1] = [img.shape[0], img.shape[1]]

        return candidates

    def buildBatch(self, images):
        X_train = np.vstack([preprocess_read_image(img) for img in images]).astype(np.int8)
        return X_train.astype(np.float32)

    def getFishProbability(self, images):
        batch = self.buildBatch(images)
        features = self.imageNet.fowardPass(batch)
        features = np.reshape(features,(np.shape(features)[0],np.shape(features)[1]*np.shape(features)[2]*np.shape(features)[3]))
        #features = np.divide(features, features.max())
        return self.svm.predict(features), []#, self.svm.predict(features)



class RandomBoxes():

    def getPredictions(self, img):
        num_boxes = np.random.randint(1, 6)
        predictions = []
        for i in range(num_boxes):
            predictions.append(self.generateBox(img))
        return np.array(predictions)
 
    def generateBox(self, img):
        p1 = [np.random.randint(0, img.shape[0]), np.random.randint(0, img.shape[1])]
        p2 = [np.random.randint(p1[0], img.shape[0]), np.random.randint(p1[1], img.shape[1])]
        return [p1, p2]
