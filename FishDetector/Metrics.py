from AnnotationLoader import *
#from sklearn.metrics.pairwise import manhattan_distances
import numpy as np


class Prediction(): 
    def __init__(self, filename, coordinates):
        self.filename = filename
        self.coordinates = coordinates

'''
    predictions is an array of objects of the form:
        {filename: string, boundingBoxes: BBox[]}
    Bboxes are numpy arrays of the form [[y1, x1],[y2, x2]] where y1, x1 are coordinates for the upper left point of the bounding box and y2, x2 the lower right point
'''
class ObjectDetectorPrecisison():

    def __init__(self, annotationLoader=None):
        if (annotationLoader == None):
            self.annotationLoader = AnnotationLoader()
        else: self.annotationLoader = annotationLoader
    
    def calculate(self, predictions):
        total_score = 0
        for i in range(len(predictions)):
            bboxes = self.annotationLoader.getAnnotationByFilename(predictions[i].filename).getBBoxes()
            score = 0
            bboxes1, bboxes2 = self.equalizeMatrixLengths(bboxes, np.array(predictions[i].coordinates))
            for j in range(bboxes1.shape[0]):
                for l in range(bboxes2.shape[0]):
                    score += self.manhattan_distances(bboxes1[j], bboxes2[l])
            total_score += float(score / len(bboxes))
        print(total_score)
        return total_score
                
    def manhattan_distances(self, bbox1, bbox2):
        return self.manhattan_distance(bbox1[0], bbox2[0]) + self.manhattan_distance(bbox1[1], bbox2[1])
      
    def manhattan_distance(self, p1, p2):
        return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])
    
    def equalizeMatrixLengths(self, m1, m2):
        if (m1.shape[0] >= m2.shape[0]):
            newM1 = m1
            newM2 = np.zeros((m1.shape[0], 2, 2))
            for i in range(m2.shape[0]):
                newM2[i] = m2[i]
        elif (m1.shape[0] < m2.shape[0]):
            newM2 = m2
            newM1 = np.zeros((m2.shape[0], 2, 2))
            for i in range(m1.shape[0]):
                newM1[i] = m1[i]
        return newM1, newM2
        
        
#ObjectDetectorPrecisison().calculate([Prediction('ALB/img_00003.jpg', np.array([[[0,0],[100,200]],[[200,200],[700,700]]]))])
        
