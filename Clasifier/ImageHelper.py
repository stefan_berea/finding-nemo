import json
import cv2
import os, glob
import utils
import csv
import numpy


###
def getFilesInFolder(folderName):
    return sorted(glob.glob(folderName + '/*'))

###
def readJSONFile(filePath):
    with open(filePath) as json_data:
        jsonData = json.load(json_data)
        return jsonData

###
def extractBoxesAndSave(fileIndex, boundingBoxAnnotation, isTrainData=True):
    filename = boundingBoxAnnotation["filename"]
    annotations = boundingBoxAnnotation["annotations"]
    count = 0

    for index, item in enumerate(annotations):
        x = int(item["x"])
        y = int(item["y"])
        height = int(item["height"])
        width = int(item["width"])
        itemClass = item["class"]
        saveBBox(x, y, height, width, itemClass, str(filename), index, fileIndex, isTrainData)
###
def getMaxLength(height, width):
    maxLength = 0
    if(height > width):
        maxLength = height
    else: 
        maxLength = width
    return maxLength

###
def saveBBox(x, y, height, width, itemClass, filename, index, fileIndex, isTrainData):
    img = cv2.imread( "../data/" + filename)
    if(height > width):
        lb = x - height//2 + width//2
        ub = x + height//2 + width//2
        if(lb < 0):
            lb = 0 
            ub = height
        if(ub > img.shape[1]):
            lb = img.shape[1] - height
            ub = img.shape[1]
        crop_img = img[y:y + height, lb:ub] # Crop from x, y, w, h
    else:
        lb = y + height//2 - width//2
        ub = y + height//2 + width//2
        if(lb < 0):
            lb = 0 
            ub = width
        if(ub > img.shape[0]):
            lb = img.shape[0] - width
            ub = img.shape[0]
        crop_img = img[lb:ub, x:x + width] # Crop from x, y, w, h

    # NOTE: it's img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]


    if (isTrainData):
        destinationFolder = "../cropped/train/"
    else:
        destinationFolder = "../cropped/validation/"
    folderName = filename.split("/")[0]
    
    for i in range(8):
        try:
            M = cv2.getRotationMatrix2D((crop_img.shape[0]/2,crop_img.shape[1]/2), i * 45, 1)
            dst = cv2.warpAffine(crop_img,M,(crop_img.shape[0],crop_img.shape[1]))
            cv2.imwrite(destinationFolder + "/" + str(filename) + "_" + str(index) + "_" + str(i) + ".png", dst)
        except:
            pass

    # print(destinationFolder + str(folderName) + "/" + str(filename) + "_" + str(index) + ".png")
    # cv2.imwrite(destinationFolder + "/" + str(filename) + "_" + str(index) + ".png", crop_img)


###
def cropFishFromImages():
    files = getFilesInFolder("boundingBoxes")
    for index in range(len(files)): 
        boundingBoxAnnotations = readJSONFile(files[index]) # Replace index with a for-loop to read all .json files

        boat_classes = {}
        with open('boat_id.csv', 'rb') as csvfile:
            for row in csv.reader(csvfile, delimiter=','):
                boat_classes[row[1]] = int(row[0])
    
        nsplits = 2
        seed=8
        # for i in range(nsplits):
        numpy.random.seed(seed)
        seed += 1
        val_classes = numpy.random.choice(range(67), size=8, replace=False)

            # val_names = [name for i, name in enumerate(names_train) if boat_classes[name] in val_classes]
            # train_names = [name for i, name in val_names if name not in val_names]
            # # train_names = np.array(list(set(xrange(N)) - set(val_names)))

        for fileIndex, item in enumerate(boundingBoxAnnotations):
            filename = item["filename"].split("/")[1]
            isTrainData = True
            if boat_classes[filename] in val_classes:
                isTrainData = False
            print(item["filename"])
            extractBoxesAndSave(fileIndex, item, isTrainData)

if __name__ == '__main__':
    cropFishFromImages()