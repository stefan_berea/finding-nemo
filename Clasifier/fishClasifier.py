from __future__ import print_function

import json
import cv2
import os, glob
import numpy as np
import numpy.random as rand
from utils import preprocess_image
from utils import get_cv_splits

import sys
import os
import time

import theano
import theano.tensor as T

import lasagne
from lasagne.regularization import regularize_layer_params, l2, l1

def load_dataset(dataSetPath):
    names_train = []

    folders = ["ALB", "BET", "DOL", "LAG", "OTHER", "SHARK", "YFT"]
 
    X_test = np.ndarray((0, 3, 224, 224)) 
    X_train = np.zeros((0, 3, 224, 224), dtype=np.int8) 
    Y_train = np.zeros((0)) 
    print(folders)
    i = 0

    last_idx = 0
    for n, dir in enumerate(folders):
        print(dir)
        folder = os.path.join(dataSetPath, dir)
        paths = sorted(glob.glob(folder + '/*'))

        samples = np.vstack([preprocess_image(p) for p in paths]).astype(np.int8)

        if dir == "test_stg1":
            X_test = samples 
        else:
            last_idx = len(paths)
            X_train = np.append(X_train, samples, axis=0)
            Y_train = np.append(Y_train, i*np.ones(len(samples)), axis=0)
            names_train.extend([p[p.rfind("/")+1:] for p in paths])
            i += 1

        samples = 0    

    return X_train.astype(np.int8), Y_train.astype(np.int32), names_train, X_test.astype(np.int8)

###########

def build_cnn(input_var=None, nfilters = 32):
    network = lasagne.layers.InputLayer(shape=(None, 3, 224, 224),
                                        input_var=input_var)
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=nfilters, filter_size=(7, 7),
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))
    network = lasagne.layers.DropoutLayer(network, p=0.2)

    network = lasagne.layers.Conv2DLayer(
            network, num_filters=64, filter_size=(7, 7),
            nonlinearity=lasagne.nonlinearities.rectify)
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))
    network = lasagne.layers.DropoutLayer(network, p=0.3)

    network = lasagne.layers.DenseLayer(num_units=128,
            nonlinearity=lasagne.nonlinearities.rectify)
    network = lasagne.layers.DropoutLayer(network, p=0.5)
    network = lasagne.layers.DenseLayer(network , num_units=64, nonlinearity=lasagne.nonlinearities.rectify)

    network = lasagne.layers.DenseLayer(num_units=8,
            nonlinearity=lasagne.nonlinearities.softmax)

    return network


# ############################# Batch iterator ###############################
def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


# ############################## Main program ################################

def main(ntrain = 50000, nvalid = 10000, ntest = 10000, algorithm_type = 1,
         batch_size_train = 500, batch_size_valid = 500, batch_size_test = 500,
         num_epochs=500, stat_filename = 'stat.txt', LR = 0.1, M = 0.9,
         nfilters = 32, time_limit = 10000):
    # Load the dataset
    print("Loading data...")
    X, Y, names_train, X_test = load_dataset('../cropped/train/')
    X_val, Y_val, names_train_val, X_test_val = load_dataset('../cropped/validation/')
    
    # Prepare Theano variables for inputs and targets
    input_var = T.tensor4('inputs')
    target_var = T.ivector('targets')

    # Create neural network model (depending on first command line parameter)
    print("Building model and compiling functions...")
    network = build_cnn(input_var, nfilters)

    # Create a loss expression for training, i.e., a scalar objective we want
    # to minimize (for our multi-class problem, it is the cross-entropy loss):
    prediction = lasagne.layers.get_output(network)
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
    # loss = loss.mean()
    # We could add some weight decay as well here, see lasagne.regularization.
    # l2_penalty = regularize_layer_params(network, l2) 
    l1_penalty = regularize_layer_params(network, l1) * 1e-4
    # loss = loss + l2_penalty + l1_penalty
    loss = loss.mean()

    # Create update expressions for training
    params = lasagne.layers.get_all_params(network, trainable=True)

    updates = lasagne.updates.adam(loss, params)

    # Create a loss expression for validation/testing
    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                            target_var)
    test_loss = test_loss.mean()
    # As a bonus, also create an expression for the classification accuracy:
    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                    dtype=theano.config.floatX)

    # Compile a function performing a training step on a mini-batch (by giving
    # the updates dictionary) and returning the corresponding training loss:
    train_fn = theano.function([input_var, target_var], loss, updates=updates)

    # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

    # Finally, launch the training loop.
    nparameters = lasagne.layers.count_params(network, trainable=True)
    print("Number of parameters in model: {}".format(nparameters))
    print("Starting training...")

    stat_file = open(stat_filename, 'w+', 0)
    start_time = time.time()

    best_val_acc = 0

    # We iterate over epochs:
    for epoch in range(num_epochs):
        # In each epoch, we do a full pass over the training data:
        train_err = 0
        train_batches = 0
        start_time_epoch = time.time()
        for batch in iterate_minibatches(X, Y, batch_size_train, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1

        # # And a full pass over the validation data:
        val_err = 0
        val_acc = 0
        val_batches = 0
        for batch in iterate_minibatches(X_val, Y_val, batch_size_valid, shuffle=False):
            inputs, targets = batch
            err, acc = val_fn(inputs, targets)
            val_err += err
            val_acc += acc
            val_batches += 1

        # Then we print the results for this epoch:
        print("Epoch {} of {} took {:.3f}s".format(epoch + 1, num_epochs, time.time() - start_time_epoch))
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
        print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
        print("  validation accuracy:\t\t{:.2f} %".format(val_acc / val_batches * 100))

        if (val_acc / val_batches * 100 > best_val_acc):
            best_val_acc = val_acc / val_batches * 100

        stat_file.write("{}\t{:.15g}\t{:.15g}\t{:.15g}\t{:.15g}\n".format(
            epoch, time.time() - start_time, train_err / train_batches,
            val_err / val_batches, val_acc / val_batches * 100))

        if (time.time() - start_time > time_limit):
            break

    # # After training, we compute and print the test error:
    # test_err = 0
    # test_acc = 0
    # test_batches = 0
    # for batch in iterate_minibatches(X_test, Y_test, batch_size_test, shuffle=False):
    #     inputs, targets = batch
    #     err, acc = val_fn(inputs, targets)
    #     test_err += err
    #     test_acc += acc
    #     test_batches += 1
    # print("Final results:")
    # print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    # print("  test accuracy:\t\t{:.2f} %".format(
    #     test_acc / test_batches * 100))

    stat_file.close()

    return [best_val_acc, time.time()-start_time, nparameters]

    # Optionally, you could now dump the network weights to a file like this:
    # np.savez('model.npz', *lasagne.layers.get_all_param_values(network))
    #
    # And load them again later on like this:
    # with np.load('model.npz') as f:
    #     param_values = [f['arr_%d' % i] for i in range(len(f.files))]
    # lasagne.layers.set_all_param_values(network, param_values)




#
if __name__ == '__main__':

    ntrain = 100              # number of training examples
    nvalid = 100              # number of validation examples
    ntest = 100               # number of test examples
    batch_size_train = 50      # batch size used for training
    batch_size_valid = 50      # batch size used for validation
    batch_size_test = 50       # batch size used for testing
    algorithm_type = 1          # 1 - SGD, 2 - SGD with momentum

    M = 0.9                     # momentum factor for SGD with momentum
    nfilters = 32               # number of convolutional filters in each layer
    time_limit = 10000          # .. seconds, huge value to disable this option

    ntrain = 500           # reduce the size of the training set by a factor of 10 to save 10x time
    nvalid = 100           # same here
    ntest = 100            # same here
    num_epochs = 50

    irun = 1              # independent runs
    LR = 0.001            # different settings for learning rate LR of SGD
    stat_filename = "stat_{}_{}.txt".format(irun, int(LR*1000))
    main(ntrain, nvalid, ntest, algorithm_type, batch_size_train,
            batch_size_valid, batch_size_test, num_epochs, stat_filename, LR, M, nfilters, time_limit)

