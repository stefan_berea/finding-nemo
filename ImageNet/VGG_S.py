# VGG_CNN_S, model from the paper:
# "Return of the Devil in the Details: Delving Deep into Convolutional Nets"
# 13.1% top-5 error on ILSVRC-2012-val
# Original source: https://gist.github.com/ksimonyan/fd8800eeb36e276cd6f9
# License: non-commercial use only

# Download pretrained weights from:
# https://s3.amazonaws.com/lasagne/recipes/pretrained/imagenet/vgg_cnn_s.pkl

from lasagne.layers import DenseLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import InputLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.layers import NonlinearityLayer
# from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.layers import Conv2DLayer as ConvLayer
from lasagne.layers import MaxPool2DLayer as PoolLayer
from lasagne.nonlinearities import softmax
from lasagne.layers import set_all_param_values

import numpy as np
import matplotlib.pyplot as plt
import pickle
import urllib
import lasagne

import theano
import io
#import skimage.transform
from utils import *

def prep_image(url):
    ext = url.split('.')[-1]
    im = plt.imread(io.BytesIO(urllib.urlopen(url).read()), ext)
    # Resize so smallest dim = 256, preserving aspect ratio
    h, w, _ = im.shape
    if h < w:
        im = skimage.transform.resize(im, (256, w*256/h), preserve_range=True)
    else:
        im = skimage.transform.resize(im, (h*256/w, 256), preserve_range=True)

    # Central crop to 224x224
    h, w, _ = im.shape
    im = im[h//2-112:h//2+112, w//2-112:w//2+112]
    
    rawim = np.copy(im).astype('uint8')
    
    # Shuffle axes to c01
    im = np.swapaxes(np.swapaxes(im, 1, 2), 0, 1)
    
    # Convert to BGR
    im = im[::-1, :, :]

    im = im - MEAN_IMAGE
    return rawim, float(im[np.newaxis])

def build_model():
    net = {}

    net['input'] = InputLayer((None, 3, 224, 224))
    net['conv1'] = ConvLayer(net['input'],
                             num_filters=96,
                             filter_size=7,
                             stride=2)
    # caffe has alpha = alpha * pool_size
    net['norm1'] = NormLayer(net['conv1'], alpha=0.0001)
    net['pool1'] = PoolLayer(net['norm1'],
                             pool_size=3,
                             stride=3,
                             ignore_border=False)
    net['conv2'] = ConvLayer(net['pool1'],
                             num_filters=256,
                             filter_size=5)
    net['pool2'] = PoolLayer(net['conv2'],
                             pool_size=2,
                             stride=2,
                             ignore_border=False)
    net['conv3'] = ConvLayer(net['pool2'],
                             num_filters=512,
                             filter_size=3,
                             pad=1)
    net['conv4'] = ConvLayer(net['conv3'],
                             num_filters=512,
                             filter_size=3,
                             pad=1)
    net['conv5'] = ConvLayer(net['conv4'],
                             num_filters=512,
                             filter_size=3,
                             pad=1)
    net['pool5'] = PoolLayer(net['conv5'],
                             pool_size=3,
                             stride=3,
                             ignore_border=False)
    net['fc6'] = DenseLayer(net['pool5'], num_units=4096)
    net['drop6'] = DropoutLayer(net['fc6'], p=0.5)
    net['fc7'] = DenseLayer(net['drop6'], num_units=4096)
    net['drop7'] = DropoutLayer(net['fc7'], p=0.5)
    net['fc8'] = DenseLayer(net['drop7'], num_units=1000, nonlinearity=None)
    net['prob'] = NonlinearityLayer(net['fc8'], softmax)

    return net

#########


output_layer = build_model()['prob']


#featuresFunction = theano.function(im, outputs=features)

model = pickle.load(open('vgg_cnn_s.pkl'))
CLASSES = model['synset words']
MEAN_IMAGE = model['mean image']

set_all_param_values(output_layer, model['values'])

#####################

#index = urllib.urlopen('http://www.image-net.org/challenges/LSVRC/2012/ori_urls/indexval.html').read()
#image_urls = index.split('<br>')

#np.random.seed(23)
#np.random.shuffle(image_urls)
#image_urls = image_urls[:5]

def number_batches(data,batch_size):
    n_samples = data.shape[0]
    n_batches = n_samples // batch_size
    if ((n_samples % batch_size) <> 0) :
        n_batches = n_batches+1
    return n_batches


img_in , img_lab , img_name , img_test = load_dataset()
batch_size = 20
n_batches = number_batches(img_in,batch_size)

all_features = []
all_labels = img_lab
all_names = img_name
#
for b in range(n_batches):
	print(b)
	batch_begin = b*batch_size
	if ((batch_begin+batch_size)>img_in.shape[0]):
		batch_end = img_in.shape[0]
	else :
		batch_end = batch_begin+batch_size
	img_in_batch = img_in[batch_begin:batch_end].astype(np.float32)
	img_lab_batch = img_lab[batch_begin:batch_end]
	features = lasagne.layers.get_output( lasagne.layers.get_all_layers(output_layer)[9] , img_in_batch ).eval()
	if b == 0:
		all_features = features
	else:
		all_features = np.append(all_features, features, axis=0)



print(np.shape(all_features))
print(np.shape(all_labels))	
print(np.shape(all_names))

np.save('features.out', all_features)
np.save('labels.out',all_labels)
np.save('names.out',all_names)

