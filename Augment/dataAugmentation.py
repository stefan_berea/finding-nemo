import sys
import os
import math
import numpy as np
from random import randint
from random import uniform
from scipy import ndimage
from PIL import Image
import scipy.ndimage.filters
import cv2

class DataAugmentor():
    def __init__(self, rotate, resize):
        self.rotate = rotate
        self.resize = resize
        self.result_size = 10000

    def start1(self, foregroundImageFolder, backgroundImageFolder, targetDir):
        size = 0
        for foregroundFilename in os.listdir(foregroundImageFolder):
            path = os.path.join(foregroundImageFolder, foregroundFilename)
            rawForeground = cv2.imread(os.path.join(foregroundImageFolder, foregroundFilename), cv2.IMREAD_UNCHANGED)
            rawForeground = self.createAlphaChannel(rawForeground)
            for backgroundFilename in os.listdir(backgroundImageFolder):
                if(size > self.result_size):
                    return
                foreground = rawForeground
                if(self.resize == True):
                    foreground = self.resizeForeground(foreground)
                if(self.rotate == True):
                    foreground = self.rotateForeground(foreground)
                background = cv2.imread(os.path.join(backgroundImageFolder, backgroundFilename), cv2.IMREAD_UNCHANGED)
                # x_offset = randint(0, background.shape[1] - foreground.shape[1])
                # y_offset = randint(0, background.shape[0] - foreground.shape[0])
                # background[y_offset:y_offset + foreground.shape[0], x_offset:x_offset + foreground.shape[1]] = foreground
                
                
                # This removes tha black baground but reduces image opacity...
                background = self.augmentBackground(foreground, background)
                self.saveImage(background, os.path.join(targetDir, self.generateFilename(foregroundFilename, backgroundFilename)))
                size += 1
                
                #cv2.namedWindow('Display image')          ## create window for display
                #cv2.imshow('Display image', background)
                #cv2.waitKey(10000)
                
    def start2(self, foregroundImageFolder, backgroundImageFolder, targetDir):
        size = 10000
        i = 0
        list_fg = os.listdir(foregroundImageFolder)
        list_bg = os.listdir(backgroundImageFolder)
        N_fg = np.shape(list_fg)[0]
        N_bg = np.shape(list_bg)[0]
        print(N_fg,N_bg)
        while ( i < size ) :
            fg_id = randint(0, N_fg)
            bg_id = randint(0, N_bg)
            path = os.path.join(foregroundImageFolder, list_fg[fg_id])
            rawForeground = cv2.imread(os.path.join(foregroundImageFolder, list_fg[fg_id]), cv2.IMREAD_UNCHANGED)
            rawForeground = self.createAlphaChannel(rawForeground)
            
            foreground = rawForeground
            if(self.resize == True):
                foreground = self.resizeForeground(foreground)
            if(self.rotate == True):
                foreground = self.rotateForeground(foreground)
            background = cv2.imread(os.path.join(backgroundImageFolder, list_bg[bg_id]), cv2.IMREAD_UNCHANGED)
                
                
            # This removes tha black baground but reduces image opacity...
            background = self.augmentBackground(foreground, background)
            self.saveImage(background, os.path.join(targetDir, self.generateFilename(list_fg[fg_id], list_bg[bg_id])))
            i += 1
                
                
    def start3(self, train_Image,train_label, PatchesFolder, targetDir):
        size = 2
        i = 0
        list_fg = os.listdir(PatchesFolder)
        list_bg = train_Image
        #print(np.shape(list_bg))
        list_bg = np.reshape(list_bg , (list_bg.shape[0],list_bg.shape[3],list_bg.shape[2],list_bg.shape[1]))
        # transparent the patches
        
        N_fg = np.shape(list_fg)[0]
        N_bg = np.shape(list_bg)[0]
        #print(N_fg,N_bg)
        new_train_Image = list_bg
        new_train_label = train_label
        print('before augment nofish = ',np.shape(np.where(np.array(new_train_label)==0)))
        print('before augment fish = ',np.shape(np.where(np.array(new_train_label)==1)))
        while ( i < size ) :
            print(i)
            fg_id = randint(0, N_fg-1)
            bg_id = randint(0, N_bg-1)
            path = os.path.join(PatchesFolder, list_fg[fg_id])
            #rawForeground = cv2.imread(os.path.join(PatchesFolder, list_fg[fg_id]), cv2.IMREAD_UNCHANGED)
            #rawForeground = self.createAlphaChannel(rawForeground)
            rawForeground = self.transparentPatch (os.path.join(PatchesFolder, list_fg[fg_id]))
            rawForeground = np.array(rawForeground)
            foreground = rawForeground
            #print(np.shape(rawForeground))
            #print(type(rawForeground))
            if(self.resize == True):
                foreground = self.resizeForeground(foreground)
            if(self.rotate == True):
                foreground = self.rotateForeground(foreground)
            background = list_bg[bg_id]
                
            #print('background : ', np.shape(background))
            #print('foreground : ', np.shape(foreground))    
            # This removes tha black baground but reduces image opacity...
            background = self.augmentBackground(foreground, background)
            #self.saveImage(background, os.path.join(targetDir, self.generateFilename(list_fg[fg_id], list_bg[bg_id])))
            self.saveImage(background, os.path.join(targetDir, str(i)+'.png'))
            #print('background : ', np.shape(background))
            #print('new_train_Image : ', np.shape(new_train_Image))
            background = np.reshape(background,(1,background.shape[0],background.shape[1],background.shape[2]))
            #print('background : ', np.shape(background))
            new_train_Image = np.append(new_train_Image,background , axis=0)
            new_train_label = np.append(new_train_label, [1] , axis=0)
            i += 1
        #print(np.shape(new_train))
        #print(np.shape(new_train_label))
        print('after augment1 nofish = ',np.shape(np.where(np.array(new_train_label)==0)))
        print('after augment1 fish = ',np.shape(np.where(np.array(new_train_label)==1)))
        nofish_idx = np.where(np.array(new_train_label)==0)
        nofish_idx = np.reshape(nofish_idx,(np.shape(nofish_idx)[1],1))
        #print(np.shape(nofish_idx))
        #print(np.shape([new_train_Image[nofish_idx[4]]]))
	for i in range (np.shape(nofish_idx)[0]-350):
            print(i)
            new_train_Image = np.append(new_train_Image,new_train_Image[nofish_idx[i]] , axis=0)
            new_train_label = np.append(new_train_label, [0] , axis=0)
        
        print('after augment2 nofish = ',np.shape(np.where(np.array(new_train_label)==0)))
        print('after augment2 fish = ',np.shape(np.where(np.array(new_train_label)==1)))
        return new_train_Image,new_train_label                         

    def start4(self, train_Image,train_label, PatchFolder_name, targetDir):
        size = 500
        folders = [ "ALB", "BET", "DOL", "LAG", "NoF", "OTHER", "SHARK", "YFT"]
        class_label = int(np.where(np.array(folders)==PatchFolder_name)[0])
        print("class label = " , class_label)
        indx = np.where(np.array(train_label)==class_label)
        if(np.shape(indx)[1]>0 ) :
            indx = np.reshape(indx,(np.shape(indx)[1],))
            train_Image = np.array(train_Image)[indx[:]]
            list_bg = train_Image
            print(np.shape(list_bg))
            list_bg = np.reshape(list_bg , (list_bg.shape[0],list_bg.shape[3],list_bg.shape[2],list_bg.shape[1]))

            fg_path = "../cropped/train/"+PatchFolder_name
            if not os.path.exists(fg_path): #NoF
                for j in range(2) :
                    for bg_id in range(np.shape(list_bg)[0]) :
                       background = list_bg[bg_id]
                       background = np.asarray(background * 255.0, dtype=int)
                       destinationFolder = targetDir+"/"+PatchFolder_name
                       if not os.path.exists(destinationFolder):
                           os.mkdir(destinationFolder)
                       self.saveImage(background, os.path.join(destinationFolder+"/", str(bg_id)+"_"+str(j)+'.png'))
                return

            list_fg = os.listdir(fg_path)
            #print(np.shape(train_Image))

            
            N_fg = np.shape(list_fg)[0]
            N_bg = np.shape(list_bg)[0]
            print(N_fg,N_bg)
            i = 0
            while ( i < size ) :
               print(i)
               fg_id = randint(0, N_fg-1)
               bg_id = randint(0, N_bg-1)
               path = os.path.join(fg_path, list_fg[fg_id])
               rawForeground = self.transparentPatch (os.path.join(fg_path, list_fg[fg_id]))
               rawForeground = np.array(rawForeground)
               foreground = rawForeground

               if(self.resize == True):
                   foreground = self.resizeForeground(foreground)
               if(self.rotate == True):
                   foreground = self.rotateForeground(foreground)
               background = list_bg[bg_id]  
               # This removes tha black baground but reduces image opacity...
               background = self.augmentBackground(foreground, background)
               destinationFolder = targetDir+"/"+PatchFolder_name
               if not os.path.exists(destinationFolder):
                   os.mkdir(destinationFolder)
               self.saveImage(background, os.path.join(destinationFolder+"/", str(i)+'.png'))
               i += 1

            i = 0
            while ( i < N_bg ) :
               print(i)
               background = list_bg[i]  
               background = np.asarray(background * 255.0, dtype=int)
               destinationFolder = targetDir+"/"+PatchFolder_name
               self.saveImage(background, os.path.join(destinationFolder+"/", str(i)+'_.png'))
               i += 1
        


                
    def transparentPatch(self, image_path):
        img = Image.open(image_path)
        #print('img : ', np.shape(img))
        #img = cv2.imread(image_path , cv2.IMREAD_UNCHANGED)
        #print('img2 : ',np.shape(img))
        img = img.convert('RGBA')
        #img = self.createAlphaChannel(img)
        p = img.load()
        width, height = img.size
        q =  np.zeros((width,height))
        q[int(width/2),int(height/2)] = 1
        gfilter = scipy.ndimage.filters.gaussian_filter(q,sigma=[int(width/3),int(height/3)])
        gfilter = (gfilter / np.max(gfilter))*255
        #print(np.min(gfilter))
        #print(np.max(gfilter))
        for y in range(height):
            for x in range(width):
                t = list(p[x,y])
                t[3] = int(gfilter[x,y])
                p[x,y] = tuple(t)
        img.save("current_patch.png")
        return img

    def rotateForeground(self, image):
        return ndimage.rotate(image, randint(1, 359))

    def resizeForeground(self, image):
        factor = uniform(0.3, 1)
        #return cv2.resize(image, (0,0), fx=factor, fy=factor) 
        return cv2.resize(image,(int(image.shape[1]/5),int(image.shape[0]/5)))

    def resizeForeground2(self, image):
        width, height = image.size
        return cv2.resize(image, (int(height/10),int(width/10))) 

    def augmentBackground(self, foreground, background):
        x_offset = randint(0, background.shape[1] - foreground.shape[1])
        y_offset = randint(0, background.shape[0] - foreground.shape[0])
        background = np.asarray(background * 255.0, dtype=int)
        #print('max foreground :', np.max(foreground))
        #print('max background :', np.max(background))
        for c in range(0,3):
            background[y_offset:y_offset + foreground.shape[0], x_offset:x_offset + foreground.shape[1], c] = \
                foreground[:, :, c] * (foreground[:, :, 3] / 255.0) +  background[y_offset:y_offset + foreground.shape[0], x_offset:x_offset + foreground.shape[1], c] * (1.0 - foreground[:,:,3] / 255.0)
        return background
            
    def saveImage(self, image, filepath):
        cv2.imwrite(filepath, image)

    def createAlphaChannel(self, image):
        if(image.shape[2] < 4):
            imageWithAlpha = np.zeros((image.shape[0], image.shape[1], image.shape[2] + 1))
            for i0 in range(0, image.shape[0]):
                for i1 in range(0, image.shape[1]):
                    for i2 in range(0, image.shape[2] + 1):
                        if (i2 == 3):
                            imageWithAlpha[i0, i1, i2] = 255
                        else:
                            imageWithAlpha[i0, i1, i2] = image[i0, i1 ,i2]
            return imageWithAlpha
        return image
            
    def generateFilename(self, foregroundFilename, backgroundFilename):
        split = foregroundFilename.split(".")
        return split[0] + "_" + backgroundFilename

if __name__ == "__main__":
    startProgramm = True
    if len(sys.argv) < 3:
        print("Usage : python dataAugmentation.py <foreground_image_dir> <background_image_dir> <target_dir>")
        startProgramm = False
    try:
        import cv2
    except:
        startProgramm = False
        print("OpenCV ist not installed on your machine! Please install it before running this programm.")
    
    if(startProgramm):
        augmentor = DataAugmentor(True, True)
        augmentor.start4(sys.argv[1], sys.argv[2], sys.argv[3])
