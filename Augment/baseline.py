import time

import numpy as np
import theano
import theano.tensor as T
import lasagne
import pickle
from utils import *
import os

from lasagne.layers import DenseLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import InputLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.layers import Conv2DLayer as ConvLayer
from lasagne.layers import MaxPool2DLayer as PoolLayer
from lasagne.nonlinearities import softmax
#from lasagne.objectives import categorical_accuracy
from sklearn.metrics import accuracy_score

from plot_fig import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class BaselineNetwork:
    def build_cnn(self, input_var=None):
        net = {}

        net = lasagne.layers.InputLayer(shape=(None, 3, 224, 224),input_var=input_var)   
        #l_in_Drop = lasagne.layers.DropoutLayer(l_in, p=0.0)

        #, stride=3   pad=1,
        net = lasagne.layers.Conv2DLayer(net , num_filters=64, filter_size=(7,7),nonlinearity=lasagne.nonlinearities.rectify)  
        net = lasagne.layers.MaxPool2DLayer(net, pool_size=(2,2))
        net = lasagne.layers.DropoutLayer(net, p=0.3)

        #stride=1,  pad=0,
        net = lasagne.layers.Conv2DLayer(net , num_filters=32, filter_size=(5,5),nonlinearity=lasagne.nonlinearities.rectify)
        net = lasagne.layers.MaxPool2DLayer(net, pool_size=(2,2))
        net = lasagne.layers.DropoutLayer(net, p=0.3)

        net = lasagne.layers.DenseLayer(net , num_units=256, nonlinearity=lasagne.nonlinearities.rectify)
        net = lasagne.layers.DropoutLayer(net, p=0.5)
        net = lasagne.layers.DenseLayer(net , num_units=64, nonlinearity=lasagne.nonlinearities.rectify)
        net = lasagne.layers.DenseLayer(net, num_units=8, nonlinearity=lasagne.nonlinearities.softmax)


        return net
            
    def number_batches(self, data, batch_size):
        n_samples = data.shape[0]
        n_batches = n_samples // batch_size
        if ((n_samples % batch_size) <> 0) :
            n_batches = n_batches + 1
        return n_batches

    def one_hot(self,labels):
        classes = np.unique(labels)
        n_classes = classes.size
        one_hot_labels = np.zeros(labels.shape + (n_classes,))
        for c in classes:
            one_hot_labels[labels == c, c] = 1
        return one_hot_labels

    def unhot(self,one_hot_labels):
        return np.argmax(one_hot_labels, axis=-1)

    def compute_accuracy(self, y_true , y_predicted):
        label = []
        #print(np.shape(y_true))
        #print(np.shape(y_predicted))
        #print(y_true)
        #print(y_predicted)
        for i in range(np.shape(y_predicted)[0]):
            #print(np.argsort(y_predicted[i])[-1])
            label = np.append(label,np.argsort(y_predicted[i])[-1])
        #print(np.shape(label))
        #print(y_true)
        #print(label)
        return accuracy_score(y_true, label)

    def main(self, num_epochs=50, LR = 0.001, time_limit = 10000):
        filter_path='./filters_baseline/'
        if not os.path.exists(filter_path):
            os.makedirs(filter_path)


        # Prepare Theano variables for inputs and targets
        input_var = T.tensor4('inputs')
        target_var = T.dmatrix('targets')

        # Load the dataset
        print("Loading data...")
        network = self.build_cnn(input_var)

        img_train , lab_train , img_name , img_test = load_dataset()
        img_train = img_train.astype(np.float32)
        img_train = np.divide(img_train, img_train.max())
        

        for train_idxs, val_idxs in get_cv_splits(img_name, nsplits=2):
            #np.random.shuffle(train_idxs)
            #np.random.shuffle(val_idxs)
            
            np.random.shuffle(train_idxs)
            img_in = img_train [train_idxs]
            img_lab = lab_train [train_idxs]
            #print("validation shape : ",np.shape(val_idxs))
            val_img_in = img_train [val_idxs]
            val_img_lab = lab_train [val_idxs]
            
            #img_in = img_in[0:50]
            #img_lab = img_lab [0:50]
            #val_img_in = val_img_in[0:30]
            #val_img_lab = val_img_lab[0:30]
            print("train shape : ",np.shape(img_in))
            print("validation shape : ",np.shape(val_img_in))
            
            prediction = lasagne.layers.get_output(network)
            #loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
            loss = lasagne.objectives.aggregate(lasagne.objectives.squared_error(prediction,target_var), mode='mean')
            #loss = loss.mean()
            
            filters_1 =lasagne.layers.get_all_params(lasagne.layers.get_all_layers(network)[1] , unwrap_shared=False)
            filterFunction_1 = theano.function([], outputs=filters_1)
            filters_2 =lasagne.layers.get_all_params(lasagne.layers.get_all_layers(network)[4] , unwrap_shared=False)
            filterFunction_2 = theano.function([], outputs=filters_2)
            params = lasagne.layers.get_all_params(network, trainable=True)
            #updates = lasagne.updates.adam(loss, params, learning_rate=LR)
            #updates = lasagne.updates.nesterov_momentum(loss, params, learning_rate=LR, momentum=0.9)
            updates = lasagne.updates.momentum(loss, params, learning_rate=LR, momentum=0.9)

            test_prediction = lasagne.layers.get_output(network, deterministic=True)
            
            #test_loss = lasagne.objectives.categorical_crossentropy(test_prediction, target_var)
            #test_loss = test_loss.mean()
            test_loss = lasagne.objectives.aggregate(lasagne.objectives.squared_error(test_prediction,target_var), mode='mean')            

            #test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
            #                 dtype=theano.config.floatX)
            #test_acc = lasagne.objectives.categorical_accuracy(test_prediction, target_var, top_k=1)

            train_fn = theano.function([input_var, target_var], loss, updates=updates)
            val_fn = theano.function([input_var, target_var], [test_loss, test_prediction])

            nparameters = lasagne.layers.count_params(network, trainable=True)
            print("Number of parameters in model: {}".format(nparameters))
            print("Starting training...")
            img_lab = self.one_hot(img_lab)
            val_img_lab = self.one_hot(val_img_lab)
            res = []
            for epoch in range(num_epochs):
                train_err = 0
                train_batches = 0
                batch_size = 10
                n_batches = self.number_batches(img_in,batch_size)
                start_time_epoch = time.time()
                for b in range(n_batches):
                    #print(b)
                    batch_begin = b*batch_size
                    if ((batch_begin+batch_size)>img_in.shape[0]):
                        batch_end = img_in.shape[0]
                    else :
                        batch_end = batch_begin+batch_size
                    img_in_batch = img_in[batch_begin:batch_end]
                    img_lab_batch = img_lab[batch_begin:batch_end]
                    #print(img_in_batch)
                    train_err += train_fn(img_in_batch, img_lab_batch)
                    train_batches += 1        

                val_err = 0
                val_acc = 0
                batch_size = 10
                val_batches = 0
                n_batches = self.number_batches(val_img_in,batch_size)
                for b in range(n_batches):
                    #print(b)
                    batch_begin = b*batch_size
                    if ((batch_begin+batch_size)>val_img_in.shape[0]):
                        batch_end = val_img_in.shape[0]
                    else :
                        batch_end = batch_begin+batch_size
                    val_img_in_batch = val_img_in[batch_begin:batch_end]
                    val_img_lab_batch = val_img_lab[batch_begin:batch_end]
                    err,  pred = val_fn(val_img_in_batch, val_img_lab_batch)
                    val_err += err
                    #print(pred)
                    val_acc += self.compute_accuracy(self.unhot(val_img_lab_batch),pred)
                    #print(val_acc)
                    val_batches += 1
                #print(pred)
                #print(self.unhot(val_img_lab_batch))
                print("Epoch {} of {} took {:.3f}s".format(
                    epoch + 1, num_epochs, time.time() - start_time_epoch))
                print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
                print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
                print("  validation accuracy:\t\t{:.2f} %".format(val_acc / val_batches * 100))
                res.append([[train_err / train_batches] , [val_err / val_batches] , [val_acc / val_batches]])
                #if ((epoch%10) == 0):
                #    filters_conv1 = filterFunction_1()
                #    filters_conv2 = filterFunction_2()
	        #    plt.figure()
	        #    plot_filters(filters_conv1[0] , epoch ,filter_path,1)
                #    plot_filters(filters_conv2[0] , epoch ,filter_path,2)
	        #    plt.show()
                #    plt.close()

            np.savetxt(filter_path+'results.dat' , res, fmt='%f\t%f\t%f\t')            

            return






if __name__ == '__main__':

    net = BaselineNetwork()
    net.main()

