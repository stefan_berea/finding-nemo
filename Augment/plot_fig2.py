import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np

data=np.loadtxt("./filters_baseline/results.dat")
train_loss = data[:,0]
valid_loss = data[:,1]
valid_acc = data[:,2]

epoch = np.arange(0,data.shape[0],1)

plt.figure(1)
plt.suptitle('Performance of the network')
plt.plot(epoch, train_loss , color="black", linewidth=1.5, linestyle="-",alpha=1.0, label="train loss")
plt.plot(epoch, valid_loss , color="green", linewidth=1.5, linestyle="-",alpha=1.0, label="validation loss")
plt.plot(epoch, valid_acc , color="orange", linewidth=1.5, linestyle="-",alpha=1.0, label="validation accuracy")

plt.legend(loc='upper right')
plt.xlabel('Epoch')
#plt.ylabel('Classification Error')
plt.ylim(0.0,1.0)
plt.savefig('fig1.png')


data=np.loadtxt("./filters_baseline_aug/results.dat")
train_loss = data[:,0]
valid_loss = data[:,1]
valid_acc = data[:,2]
epoch = np.arange(0,data.shape[0],1)

plt.figure(2)
plt.suptitle('Performance of the network')
plt.plot(epoch, train_loss , color="black", linewidth=1.5, linestyle="-",alpha=1.0, label="train loss")
plt.plot(epoch, valid_loss , color="green", linewidth=1.5, linestyle="-",alpha=1.0, label="validation loss")
plt.plot(epoch, valid_acc , color="orange", linewidth=1.5, linestyle="-",alpha=1.0, label="validation accuracy")

plt.legend(loc='upper right')
plt.xlabel('Epoch')
#plt.ylabel('Classification Error')
plt.ylim(0.0,1.0)
plt.savefig('fig2.png')
