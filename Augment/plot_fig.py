import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import theano
from theano.tensor import *
#from sklearn_theano.feature_extraction import fetch_overfeat_weights_and_biases


def make_visual(layer_weights):
    max_scale = np.max(layer_weights)
    print('max scale :' + str(max_scale))
    min_scale = np.min(layer_weights)
    return (255 * (layer_weights - min_scale) /
            (max_scale - min_scale)).astype('uint8')


def make_mosaic(layer_weights,number_row):
    lw_shape = layer_weights.shape
    lw = make_visual(layer_weights).reshape(number_row, 8, *lw_shape[1:])
    print(np.shape(lw))
    lw = lw.transpose(0, 3, 1, 4, 2)
    lw = lw.reshape(number_row * lw_shape[-1], 8 * lw_shape[-2], lw_shape[1])
    return lw


def plot_filters(lw,epoch,filter_path,layer):
    layer_weights = lw
    if(layer == 1) :
        number_row = 4
    else:
        number_row = 8
    mosaic = make_mosaic(layer_weights,number_row)
    plt.imshow(mosaic, interpolation='nearest')
    ax = plt.gca()
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    plt.savefig(filter_path+'filter_epoch_' +str(layer)+'_'+ str(epoch) + '.PNG')

