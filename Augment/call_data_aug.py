import sys
import os
import math
import numpy as np
from dataAugmentation import *
from utils import *

folders = [ "ALB", "BET", "DOL", "LAG", "NoF", "OTHER", "SHARK", "YFT"]
img_in , img_lab , img_name, img_test = load_dataset()
img_in = img_in.astype(np.float32)
img_in = np.divide(img_in, img_in.max())
for train_idxs, val_idxs in get_cv_splits(img_name, nsplits=2):
    train_img_in = img_in[train_idxs]
    train_img_lab = img_lab[train_idxs]
    val_img_in = img_in [val_idxs]
    val_img_lab = img_lab [val_idxs]
    augmentor = DataAugmentor(True, True)
    for n, dir in enumerate(folders):
        augmentor.start4(train_img_in,train_img_lab,str(dir),'./target')



