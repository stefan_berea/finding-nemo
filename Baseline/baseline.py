import time

import numpy as np
import theano
import theano.tensor as T
import lasagne
import pickle
from utils import *

from lasagne.layers import DenseLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import InputLayer
from lasagne.layers import LocalResponseNormalization2DLayer as NormLayer
from lasagne.layers import Conv2DLayer as ConvLayer
from lasagne.layers import MaxPool2DLayer as PoolLayer
from lasagne.nonlinearities import softmax

class BaselineNetwork:
    def build_cnn(self, input_var=None):
        net = {}

        net = InputLayer((None, 3, 224, 224), input_var=input_var)
        net = ConvLayer(net, num_filters=96,
                        filter_size=7,
                         stride=2)
        # caffe has alpha = alpha * pool_size
        net = NormLayer(net, alpha=0.0001)
        net = PoolLayer(net, pool_size=3,
                                 stride=3,
                                 ignore_border=False)
        net = ConvLayer(net, num_filters=256,
                                 filter_size=5)
        net = PoolLayer(net, pool_size=2,
                                 stride=2,
                                 ignore_border=False)
        net = ConvLayer(net, num_filters=128,
                                 filter_size=3,
                                 pad=1)
        net = PoolLayer(net, pool_size=3,
                                 stride=3,
                                 ignore_border=False)
        net = DenseLayer(net, num_units=20, nonlinearity=None)
        net = DenseLayer(net, num_units=8, nonlinearity=softmax)

        return net
            
    def number_batches(self, data, batch_size):
        n_samples = data.shape[0]
        n_batches = n_samples // batch_size
        if ((n_samples % batch_size) <> 0) :
            n_batches = n_batches + 1
        return n_batches

    def main(self, num_epochs=20, LR = 0.001, time_limit = 10000):
        # Prepare Theano variables for inputs and targets
        input_var = T.tensor4('inputs')
        target_var = T.ivector('targets')

        # Load the dataset
        print("Loading data...")
        network = self.build_cnn(input_var)

        img_in , img_lab , img_name , img_test = load_dataset()
        print(img_lab.shape)
        batch_size = 40
        n_batches = self.number_batches(img_in,batch_size)
        img_in = img_in.astype(np.float32)
        img_in = np.divide(img_in, img_in.max())
        all_features = []
        all_labels = img_lab
        all_names = img_name
        
        for train_idxs, val_idxs in get_cv_splits(img_name, nsplits=2):
            np.random.shuffle(train_idxs)
            np.random.shuffle(val_idxs)
            
            train_img_in = img_in [train_idxs]
            train_img_lab = img_lab [train_idxs]
            
            val_img_in = img_in [val_idxs]
            val_img_lab = img_lab [val_idxs]
            
            print("train shape : ",train_img_lab.shape)
            print("validation shape : ",val_img_lab.shape)
        
            prediction = lasagne.layers.get_output(network)
            loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
            loss = loss.mean()
            
            params = lasagne.layers.get_all_params(network, trainable=True)
            updates = lasagne.updates.adam(loss, params, learning_rate=LR)

            test_prediction = lasagne.layers.get_output(network, deterministic=True)
            
            test_loss = lasagne.objectives.categorical_crossentropy(test_prediction, target_var)
            test_loss = test_loss.mean()
            
            test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                             dtype=theano.config.floatX)

            train_fn = theano.function([input_var, target_var], loss, updates=updates)
            val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

            nparameters = lasagne.layers.count_params(network, trainable=True)
            print("Number of parameters in model: {}".format(nparameters))
            print("Starting training...")

            for epoch in range(num_epochs):
                train_err = 0
                train_batches = 0
                batch_size = 40
                n_batches = self.number_batches(img_in,batch_size)
                start_time_epoch = time.time()
                for b in range(n_batches):
                    print(b)
                    batch_begin = b*batch_size
                    if ((batch_begin+batch_size)>img_in.shape[0]):
                        batch_end = img_in.shape[0]
                    else :
                        batch_end = batch_begin+batch_size
                    img_in_batch = img_in[batch_begin:batch_end]
                    img_lab_batch = img_lab[batch_begin:batch_end]
                    train_err += train_fn(img_in_batch, img_lab_batch)
                    train_batches += 1        

                val_err = 0
                val_acc = 0
                batch_size = 20
                val_batches = 0
                n_batches = self.number_batches(val_img_in,batch_size)
                for b in range(n_batches):
                    print(b)
                    batch_begin = b*batch_size
                    if ((batch_begin+batch_size)>val_img_in.shape[0]):
                        batch_end = val_img_in.shape[0]
                    else :
                        batch_end = batch_begin+batch_size
                    val_img_in_batch = val_img_in[batch_begin:batch_end]
                    val_img_lab_batch = val_img_lab[batch_begin:batch_end]
                    err, acc = val_fn(val_img_in_batch, val_img_lab_batch)
                    val_err += err
                    val_acc += acc
                    val_batches += 1

                print("Epoch {} of {} took {:.3f}s".format(
                    epoch + 1, num_epochs, time.time() - start_time_epoch))
                print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
                print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
                print("  validation accuracy:\t\t{:.2f} %".format(val_acc / val_batches * 100))
            return

if __name__ == '__main__':
    net = BaselineNetwork()
    net.main()

