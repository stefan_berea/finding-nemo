import numpy as np
import cv2
import os, glob
import csv

def preprocess_image(path):
    img = cv2.imread(path)
    return preprocess_read_image(img)

def preprocess_read_image(img):
    rows = 224
    cols = 224
    if img is None:
        return np.zeros((1, 3, 224, 224))

    img = cv2.resize(img, (224, 224))

    img = np.swapaxes(img, 0, 2)
    img = np.swapaxes(img, 1, 2).astype(np.int8)
    #TODO: Extract real mean
    img -= 117

    return img.reshape([1, 3, 224, 224])

def load_names():
    names_train = []

    folders = ["ALB", "BET", "DOL", "LAG", "NoF", "OTHER", "SHARK", "YFT"]
 
    for n, dir in enumerate(folders):
        folder = os.path.join('../../data/', dir)
        paths = sorted(glob.glob(folder + '/*'))

        names_train.extend([p[p.rfind("/")+1:] for p in paths])

    return names_train


def load_dataset():
    names_train = []

    folders = ["ALB", "BET", "DOL", "LAG", "NoF", "OTHER", "SHARK", "YFT"]
 
    X_test = np.ndarray((0, 3, 224, 224)) 
    X_train = np.zeros((0, 3, 224, 224), dtype=np.int8) 
    y_train = np.zeros((0)) 
    print(folders)
    i = 0

    last_idx = 0
    for n, dir in enumerate(folders):
        print(dir)
        folder = os.path.join('../../data/', dir)
        paths = sorted(glob.glob(folder + '/*'))

        samples = np.vstack([preprocess_image(p) for p in paths]).astype(np.int8)

        if dir == "test_stg1":
            X_test = samples 
        else:
            last_idx = len(paths)
            X_train = np.append(X_train, samples, axis=0)
            y_train = np.append(y_train, i*np.ones(len(samples)), axis=0)
            names_train.extend([p[p.rfind("/")+1:] for p in paths])
            i += 1

        samples = 0    

    return X_train.astype(np.int8), y_train.astype(np.int32), names_train, X_test.astype(np.int8)

def get_cv_splits(names_train, seed=8, nsplits=10):
    boat_classes = {}
    with open('boat_id.csv', 'rb') as csvfile:
        for row in csv.reader(csvfile, delimiter=','):
            boat_classes[row[1]] = int(row[0])
 
    N = len(names_train)
    for i in range(nsplits):
        np.random.seed(seed)
        seed += 1
        val_classes = np.random.choice(range(67), size=8, replace=False)

        val_idxs = [i for i, name in enumerate(names_train) if boat_classes[name] in val_classes]
        train_idxs = np.array(list(set(xrange(N)) - set(val_idxs)))

        np.random.shuffle(val_idxs)
        yield train_idxs, val_idxs
